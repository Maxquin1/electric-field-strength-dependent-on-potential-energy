import pickle
import argparse
from scipy.interpolate import PchipInterpolator
import numpy as np

class PotentialEnergyCalculator:
    def __init__(self, data_file, ef_max=0.2, dE=0.01):
        self.data = np.genfromtxt(data_file, dtype=float, delimiter=' ')
        self.ef_start = np.array([0, 0, 0])
        self.ef_max = ef_max
        self.dE = dE
        self.i_interp, self.j_interp, self.k_interp = self.data_interpolation()

    def data_interpolation(self):
        ef_strength = self.data[0]
        i_direction = self.data[1]
        j_direction = self.data[2]
        k_direction = self.data[3]

        i_interp = PchipInterpolator(ef_strength, i_direction)
        j_interp = PchipInterpolator(ef_strength, j_direction)
        k_interp = PchipInterpolator(ef_strength, k_direction)

        return i_interp, j_interp, k_interp

    def central_difference(self, ef_new):
        central_i = (self.i_interp(ef_new - self.dE) + self.i_interp(ef_new + self.dE)) / (2 * self.dE)
        central_j = (self.j_interp(ef_new - self.dE) + self.j_interp(ef_new + self.dE)) / (2 * self.dE)
        central_k = (self.k_interp(ef_new - self.dE) + self.k_interp(ef_new + self.dE)) / (2 * self.dE)

        nabla_potential = np.array([central_i, central_j, central_k])
        magnitude = np.linalg.norm(nabla_potential)

        return nabla_potential, magnitude

    def unit_vector(self, nabla_potential, magnitude):
        n_hat = -nabla_potential / magnitude
        return n_hat

    def get_potential(self, ef_start):
        i_interp = self.i_interp(ef_start[0])
        j_interp = self.j_interp(ef_start[1])
        k_interp = self.k_interp(ef_start[2])

        potential = np.dot(np.array([i_interp, j_interp, k_interp]), self.n_hat)
        return potential

    def calculate_potential_energy(self):
        ef_new = 0.0

        while ef_new <= self.ef_max:
            self.nabla_potential, magnitude = self.central_difference(ef_new)
            self.n_hat = self.unit_vector(self.nabla_potential, magnitude)
            self.ef_start += self.dE * self.n_hat
            ef_new = np.linalg.norm(self.ef_start)
            potential = self.get_potential(self.ef_start)
            print(ef_new, self.ef_start, potential)

def define_arguments():
    """Define Command Line Arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-emax", "--ef_max", default=0.2, type=float,
                        help="Maximum electric field strength to develop calculations")
    parser.add_argument("-dE", "--dE", default=0.01, type=float,
                        help="Step size of the change in electric field strength")
    parser.add_argument("-data", "--data", required=True,
                        help="Path to the file with potential energy values")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = define_arguments()
    calculator = PotentialEnergyCalculator(args.data, args.ef_max, args.dE)
    calculator.calculate_potential_energy()

    


